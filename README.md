# Taler Merchant Demos Setup

_Warning: This page is not completed yet. Follow the instructions at your own risk_

## Installing the dependencies

### Automatically

The [Makefile](./Makefile) contains premade scripts for some distros;

```bash
# Arch-based (Manjaro, Arco, etc...)
$ make setup-arch

# Debian-based (Ubuntu, Mint, etc...)
$ make setup-deb
```

### Manually

<!--
  TODO: Migrate to per-distro instructions
  -> We need to consider if we should incrase the README.md's size, or if we should split said per-distro instructions into multiple files
-->

_Note: These instructions assume you are on a debian-based distribution. Other distributions may vary!_

1. Update Apt Repositories (Recommended);
   > ```bash
   > $ sudo apt update
   > ```
2. Step 2: Ensure Python 3.5 or above and Flask with the Babel extension are installed;
   > ```bash
   > $ sudo apt install python3 python-is-python3 python3-flask-babel -y
   > ```
3. Ensure Python3 Pip is installed;
   > ```bash
   > # you may need to use python3-pip on some systems
   > $ sudo apt install python3-pip -y
   > ```
4. Configure things using:
   > ```bash
   > $ ./configure --prefix=local
   > ```
5. Install UWSGI
   > ```bash
   > $ pip install uwsgi # You may need to use pip3 on some systems
   > ```
6. Install LXML
   > ```bash
   > $ pip install lxml # You may need to use pip3 on some systems
   > ```
7. Install NodeJS <!-- NOTE: Likely redundant, legacy -->
   > ```bash
   > $ sudo apt install npm nodejs
   > ```
8. Install Poetry
   > ```bash
   > $ pip install poetry
   > ```

## Configuring the demo

_This is just how I did it, and not necessarily the optimal method. You should only use this in testing._

Step 1: Open the config:

> `$ editor ~/.config/taler.conf`

<br>

Step 2: Adding the required configuration values:

> ```
> [frontends]
> backend_apikey = "secret-token:secret"
> backend = https://backend.demo.taler.net/
>
> [taler]
> currency = KUDOS
>
> [blog]
> http_port = 8080
> serve = http
> ```

<br>

Step 3: Configure the config:

> 1. Replace the backend api key with the api key for the backend<br>
> 2. Replace the backend URL with the actual backend link<br>
> 3. Replace the http port with whichever port you desire<br>
> 4. _In Production:_ Replace the currency with your currency

<br>

## Applying Changes

To apply changes, use

> `$ make install`

## Running the program

To start the server, use the following command:<br>

> `$ taler-merchant-demos blog`

_Note: If zsh asks you if you want to correct it to talermerchantdemos, select **no**_

## More configuration options.

This makes the blog speak UWSGI over TCP:

> ```
> [frontends]
> backend_apikey = "secret-token:secret"
> backend = https://backend.demo.taler.net/
>
> [taler]
> currency = KUDOS
>
> [blog]
> serve = uwsgi
> uwsgi_serve = tcp
> uwsgi_port = XZY
> ```

<br>

This makes the blog speak UWSGI over unix domain socket:

> ```
> [frontends]
> backend_apikey = "secret-token:secret"
> backend = https://backend.demo.taler.net/
>
> [taler]
> currency = KUDOS
>
> [blog]
> serve = uwsgi
> uwsgi_serve = unix
> uwsgi_unixpath = "/tmp/blog.uwsgi"
> uwsgi_unixpath_mode = XZY
> ```
