##
# This file is part of GNU Taler
# (C) 2017,2021 Taler Systems S.A.
#
# GNU Taler is free software; you can redistribute it and/or
# modify it under the terms of the GNU Affero General Public
# License as published by the Free Software Foundation; either
# version 3, or (at your option) any later version.
#
# GNU Taler is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty
# of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public
# License along with GNU Taler; see the file COPYING.  If not,
# see <http://www.gnu.org/licenses/>
#
#  @author Florian Dold
#  @file Standalone script to run the blog.

import click
import logging
import argparse
import sys
import os
import site
from .util.talerconfig import TalerConfig, ConfigurationError

import importlib
import multiprocessing
import gunicorn.app.base

LOGGER = logging.getLogger(__name__)


def bindspec(config, which_shop, port):
    confsection = f"frontend-demo-{which_shop}"

    # Takes precedence.
    if port:
        http_serve = "tcp"
    else:
        http_serve = (
            config[confsection]["http_serve"]
            .value_string(required=False, default="tcp")
            .lower()
        )

    if http_serve == "tcp":
        port_launch = (
            config[confsection]["http_port"].value_int(required=False)
            if not port
            else port
        )
        if not port_launch:
            print("Port number wasn't found in config and in arguments.", file=sys.stderr)
            exit(1)
        return dict(bind=f"0.0.0.0:{port_launch}")

    if http_serve == "unix":
        path = config[confsection]["http_unixpath"].value_filename(required=True)
        mode = config[confsection]["http_unixpath_mode"].value_filename(required=True)
        return dict(bind=f"unix:{path}", umask=mode)


def number_of_workers(config, which_shop):
    confsection = f"frontend-demo-{which_shop}"
    nw = config[confsection]["num_workers"].value_int(required=False)
    if nw is None:
        # Be conservative with number of workers
        return 2
    else:
        return nw


class StandaloneApplication(gunicorn.app.base.BaseApplication):

    def __init__(self, app, options=None):
        self.options = options or {}
        self.application = app
        super().__init__()

    def load_config(self):
        config = {
            key: value
            for key, value in self.options.items()
            if key in self.cfg.settings and value is not None
        }
        for key, value in config.items():
            self.cfg.set(key.lower(), value)

    def load(self):
        return self.application


@click.command("Global shop launcher")
@click.option("-c", "--config", 'config_filename', help="Configuration file", required=False)
@click.option(
    "--http-port",
    help="HTTP port to serve (if not given, serving comes from config)",
    required=False,
    type=int,
)
@click.argument("which-shop")
def demos(config_filename, http_port, which_shop):
    """WHICH_SHOP is one of: blog, donations, provision, or landing."""

    if which_shop not in ["blog", "donations", "provision", "landing"]:
        print("Please use a valid shop name: blog, donations, provision, landing.")
        sys.exit(1)
    config = TalerConfig.from_file(config_filename)
    options = {
        "workers": number_of_workers(config, which_shop),
        **bindspec(config, which_shop, http_port),
    }
    mod = f"talermerchantdemos.{which_shop}"
    os.environ["TALER_CONFIG_FILENAME"] = config_filename or ""
    app = importlib.import_module(mod).app
    StandaloneApplication(app, options).run()


def run():
    demos()

if __name__ == "__main__":
    run()
