<!--#include virtual="/server/header.html" -->
<!-- Parent-Version: 1.96 -->
<!-- This page is derived from /server/standards/boilerplate.html -->
<!--#set var="TAGS" value="thirdparty" -->
<!--#set var="DISABLE_TOP_ADDENDUM" value="yes" -->
<title>Why Audio Format Matters
- GNU Project - Free Software Foundation</title>
<style type="text/css" media="print,screen"><!--
h2 { margin-bottom: .1em; }
h2 + h3 { margin: 0 0 1.2em; }
--></style>
<!--#include virtual="/philosophy/po/why-audio-format-matters.translist" -->
<!--#include virtual="/server/banner.html" -->
<!--#include virtual="/philosophy/ph-breadcrumb.html" -->
<!--GNUN: OUT-OF-DATE NOTICE-->
<!--#include virtual="/server/top-addendum.html" -->
<div class="article reduced-width">
<h2>Why Audio Format Matters</h2>

<h3>An invitation to audio producers to use Ogg
Vorbis alongside MP3</h3>

<address class="byline">by Karl Fogel</address>

<div class="infobox">
<p>The patents covering MP3 will reportedly all have expired by 2018,
but similar problems will continue to arise as long as patents are
permitted to restrict software development.</p>
</div>
<hr class="thin" />
 
<p>If you produce audio for general distribution, you probably spend
99.9% of your time thinking about form, content, and production
quality, and 0.1% thinking about what audio format to distribute your
recordings in.</p>

<p>And in an ideal world, this would be fine.  Audio formats would be
like the conventions of laying out a book, or like pitches
and other building-blocks of music: containers of meaning, available
for anyone to use, free of restrictions.  You wouldn't have to worry
about the consequences of distributing your material in MP3 format,
any more than you would worry about putting a page number at the top
of a page, or starting a book with a table of contents.</p>

<p>Unfortunately, that is not the world we live in.  MP3 is a patented
format.  What this means is that various companies have
government-granted monopolies over certain aspects of the MP3
standard, such that whenever someone creates or listens to an MP3
file, <em>even with software not written by one of those
companies</em>, the companies have the right to decide whether or not
to permit that use of MP3.  Typically what they do is demand money, of
course.  But the terms are entirely up to them: they can forbid you
from using MP3 at all, if they want.  If you've been using MP3 files
and didn't know about this situation, then either a) someone else,
usually a software maker, has been paying the royalties for you, or b)
you've been unknowingly infringing on patents, and in theory could be
sued for it.</p>

<p>The harm here goes deeper than just the danger to you.  A software
patent grants one party the exclusive right to use a certain
mathematical fact.  This right can then be bought and sold, even
litigated over like a piece of property, and you can never predict
what a new owner might do with it.  This is not just an abstract
possibility: MP3 patents have been the subject of multiple lawsuits,
with damages totalling more than a billion dollars.</p>

<p>The most important issue here is not about the fees, it's about the
freedom to communicate and to develop communications tools.
Distribution formats such as MP3 are the containers of information
exchange on the Internet.  Imagine for a moment that someone had a
patent on the modulated vibration of air molecules: you would need a
license just to hold a conversation or play guitar for an audience.
Fortunately, our government has long held that old, familiar methods
of communication, like vibrating air molecules or writing symbols on
pieces of paper, are not patentable: no one can own them, they are
free for everyone to use.  But until those same liberties are extended
to newer, less familiar methods (like particular standards for
representing sounds via digital encoding), we who generate audio
works must take care what format we use&mdash;and
require our listeners to use.</p>

<h4 class="sec">A way out: Ogg Vorbis format</h4>

<p>Ogg Vorbis is an alternative to MP3.  It gets high sound quality,
can compress down to a smaller size than MP3 while still sounding good
(thus saving you time and bandwidth costs), and best of all, is
designed to be completely free of patents.</p>

<p>You won't sacrifice any technical quality by encoding your audio in
Ogg Vorbis.  The files sound fine, and most players know how to play
them.  But you will increase the total number of people who can listen
to your tracks, and at the same time help the push for patent-free
standards in distribution formats.</p>

<div class="announcement comment" role="complementary">
<p>More information <a href="https://xiph.org/about/">about Xiph.org</a> (the
organization that created Ogg Vorbis) and the importance of free
distribution formats.</p>

<p>The Free Software Foundation has produced a user-friendly <a
href="https://www.fsf.org/campaigns/playogg/how">guide to installing Ogg
Vorbis support in Microsoft Windows and Apple Mac OS X</a>.</p>
</div>

<p>The <a href="https://xiph.org/vorbis/">Ogg Vorbis home page</a>
has all the information you need to both listen
to and produce Vorbis-encoded files.  The safest thing, for you and
your listeners, would be to offer Ogg Vorbis files exclusively.  But
since there are still some players that can only handle MP3, and you
don't want to lose audience, a first step is to offer both Ogg Vorbis
and MP3, while explaining to your downloaders (perhaps by linking to
this article) exactly why you support Ogg Vorbis.</p>

<p>And with Ogg Vorbis, you'll even <em>gain</em> some audience.
Here's how:</p>

<p>Up till now, the MP3 patent owners have been clever enough not to
harass individual users with demands for payment.  They know that
would stimulate popular awareness of (and eventually opposition to)
the patents.  Instead, they go after the makers of products that
implement the MP3 format.  The victims of these shakedowns shrug
wearily and pay up, viewing it as just another cost of doing business,
which is then passed on invisibly to users.  However, not everyone is
in a position to pay: some of your listeners use free software
programs to play audio files.  Because this software is freely copied
and downloaded, there is no practical way for either the authors or
the users to pay a patent fee&mdash;that is, to pay for
the right to use the mathematical facts that underly the MP3 format.
As a result, these programs cannot legally implement MP3, even though
the tracks the users want to listen to may themselves be perfectly
free!  Because of this situation, some distributors of the GNU/Linux
computer operating system&mdash;which has millions of
users worldwide&mdash;have been unable to include MP3
players in their software distributions.</p>

<p>Luckily, you don't have to require such users to engage in civil
disobedience every time they want to listen to your works.  By
offering Ogg Vorbis, you ensure that no listeners have to get involved
with a patented distribution format unless they choose to, and that
your audio works will never be hampered by unforseen licensing
requirements.  Eventually, the growing acceptance of Ogg Vorbis as a
standard, coupled with increasingly unpredictable behavior by some of
the MP3 patent holders, may make it impractical to offer MP3 files at
all.  But even before that day comes, Ogg Vorbis remains the only
portable, royalty-free audio format on the Internet, and it's worth a
little extra effort to support.</p>
</div>

</div><!-- for id="content", starts in the include above -->
<!--#include virtual="/server/footer.html" -->
<div id="footer" role="contentinfo">
<div class="unprintable">

<p>Please send general FSF &amp; GNU inquiries to
<a href="mailto:gnu@gnu.org">&lt;gnu@gnu.org&gt;</a>.
There are also <a href="/contact/">other ways to contact</a>
the FSF.  Broken links and other corrections or suggestions can be sent
to <a href="mailto:webmasters@gnu.org">&lt;webmasters@gnu.org&gt;</a>.</p>

<p><!-- TRANSLATORS: Ignore the original text in this paragraph,
        replace it with the translation of these two:

        We work hard and do our best to provide accurate, good quality
        translations.  However, we are not exempt from imperfection.
        Please send your comments and general suggestions in this regard
        to <a href="mailto:web-translators@gnu.org">
        &lt;web-translators@gnu.org&gt;</a>.</p>

        <p>For information on coordinating and contributing translations of
        our web pages, see <a
        href="/server/standards/README.translations.html">Translations
        README</a>. -->
Please see the <a
href="/server/standards/README.translations.html">Translations
README</a> for information on coordinating and contributing translations
of this article.</p>
</div>

<p>Copyright &copy; 2007 Karl Fogel</p>

<p>Verbatim copying and distribution of this entire article are
permitted worldwide, without royalty, in any medium, provided this
notice, and the copyright notice, are preserved.</p>

<!--#include virtual="/server/bottom-notes.html" -->

<p class="unprintable">Updated:
<!-- timestamp start -->
$Date: 2021/09/22 08:18:39 $
<!-- timestamp end -->
</p>
</div>
</div><!-- for class="inner", starts in the banner include -->
</body>
</html>
