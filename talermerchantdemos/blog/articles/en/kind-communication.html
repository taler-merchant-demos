<!--#include virtual="/server/header.html" -->
<!-- Parent-Version: 1.96 -->
<!-- This page is derived from /server/standards/boilerplate.html -->
<!--#set var="TAGS" value="essays upholding action" -->
<!--#set var="DISABLE_TOP_ADDENDUM" value="yes" -->
<title>GNU Kind Communications Guidelines
- GNU Project - Free Software Foundation</title>
 <!--#include virtual="/philosophy/po/kind-communication.translist" -->
<!--#include virtual="/server/banner.html" -->
<!--#include virtual="/philosophy/ph-breadcrumb.html" -->
<!--GNUN: OUT-OF-DATE NOTICE-->
<!--#include virtual="/server/top-addendum.html" -->
<div class="article reduced-width">
<h2>GNU Kind Communications Guidelines</h2>

<address class="byline">by
<a href="https://www.stallman.org/">Richard Stallman</a></address>

<h3>Purpose</h3>

<p>The GNU Project encourages contributions from anyone who wishes to
advance the development of the GNU system, regardless of gender, race,
ethnic group, physical appearance, religion, cultural background, and
any other demographic characteristics, as well as personal political
views.</p>

<p>People are sometimes discouraged from participating in GNU
development because of certain patterns of communication that strike
them as unfriendly, unwelcoming, rejecting, or harsh. This
discouragement particularly affects members of disprivileged
demographics, but it is not limited to them.  Therefore, we ask all
contributors to make a conscious effort, in GNU Project discussions,
to communicate in ways that avoid that outcome&mdash;to avoid
practices that will predictably and unnecessarily risk putting some
contributors off.</p>

<p>These guidelines suggest specific ways to accomplish that goal.</p>

<h3>Guidelines</h3>

<ul>
  <li>Please assume other participants are posting in good faith, even if
  you disagree with what they say. When people present code or text as
  their own work, please accept it as their work.  Please do not
  criticize people for wrongs that you only speculate they may have
  done; stick to what they actually say and actually do.</li>

  <li>Please think about how to treat other participants with respect,
  especially when you disagree with them.  For instance, call them by the
  names they use, and honor their preferences about their gender
  identity<a href="#f1">[1]</a>.</li>

  <li>Please do not take a harsh tone towards other participants, and
  especially don't make personal attacks against them.  Go out of your
  way to show that you are criticizing a statement, not a person.</li>

  <li>Please recognize that criticism of your statements is not a
  personal attack on you. If you feel that someone has attacked you, or
  offended your personal dignity, please don't &ldquo;hit back&rdquo;
  with another personal attack.  That tends to start a vicious circle of
  escalating verbal aggression.  A private response, politely stating
  your feelings <em>as feelings</em>, and asking for peace, may calm
  things down.  Write it, set it aside for hours or a day, revise it to
  remove the anger, and only then send it.</li>

  <li>Please avoid statements about the presumed typical desires,
  capabilities or actions of some demographic group.  They can offend
  people in that group, and they are always off-topic in GNU Project
  discussions.</li>

  <li>Please be especially kind to other contributors when saying they
  made a mistake.  Programming means making lots of mistakes, and we all
  do so&mdash;this is why regression tests are useful.  Conscientious
  programmers make mistakes, and then fix them.  It is helpful to show
  contributors that being imperfect is normal, so we don't hold it
  against them, and that we appreciate their imperfect contributions
  though we hope they follow through by fixing any problems in them.</li>

  <li>Likewise, be kind when pointing out to other contributors that they
  should stop using certain nonfree software.  For their own sake, they
  ought to free themselves, but we welcome their contributions to our
  software packages even if they don't do that. So these reminders
  should be gentle and not too frequent&mdash;don't nag.

  <p>By contrast, to suggest that others run a nonfree program opposes
  the basic principles of GNU, so it is not allowed in GNU Project
  discussions.</p>
  </li>

  <li>Please respond to what people actually said, not to exaggerations
  of their views.  Your criticism will not be constructive if it is aimed
  at a target other than their real views.</li>

  <li>If in a discussion someone brings up a tangent to the topic at
  hand, please keep the discussion on track by focusing on the current
  topic rather than the tangent.  This is not to say that the tangent is
  bad, or not interesting to discuss&mdash;only that it shouldn't
  interfere with discussion of the issue at hand.  In most cases, it is
  also off-topic, so those interested ought to discuss it somewhere
  else.

  <p>If you think the tangent is an important and pertinent issue,
  please bring it up as a separate discussion, with a Subject field to
  fit, and consider waiting for the end of the current discussion.</p>
  </li>

  <li>Rather than trying to have the last word, look for the times when
  there is no need to reply, perhaps because you already made the
  relevant point clear enough.  If you know something about the game of
  Go, this analogy might clarify that: when the other player's move is not
  strong enough to require a direct response, it is advantageous to give
  it none and instead move elsewhere.</li>

  <li>Please don't argue unceasingly for your preferred course of action
  when a decision for some other course has already been made.  That
  tends to block the activity's progress.</li>

  <li>If others have irritated you, perhaps by disregarding these
  guidelines, please don't excoriate them, and especially please don't
  hold a grudge against them.  The constructive approach is to
  encourage and help other people to do better.  When they are trying
  to learn to do better, please give them plenty of chances.</li>

  <li>If other participants complain about the way you express your
  ideas, please make an effort to cater to them.  You can find ways to
  express the same points while making others more comfortable.  You are
  more likely to persuade others if you don't arouse ire about secondary
  things.</li>

  <li>Please don't raise unrelated political issues in GNU Project
  discussions, because they are off-topic.  The only political positions
  that the GNU Project endorses are (1) that users should have control
  of their own computing (for instance, through free software) and (2)
  supporting basic human rights in computing.  We don't require you as a
  contributor to agree with these two points, but you do need to accept
  that our decisions will be based on them.</li>
</ul>

<p>By making an effort to follow these guidelines, we will encourage
more contribution to our projects, and our discussions will be
friendlier and reach conclusions more easily.</p>
<div class="column-limit"></div>

<h3 class="footnote">Footnote</h3>

<ol>
  <li id="f1">
    <p>Honoring people's preferences about gender identity includes
       not referring to them in ways that conflict with that identity,
       and using specific pronouns for it when those exist.  If you
       know someone wishes to be considered male, it is best to use
       the masculine pronouns for him.  If you know someone wishes to
       be considered female, it is best to use the feminine pronouns
       for her.  Otherwise, use gender-neutral pronouns, since at
       least they don't conflict with anyone's gender identity.  One
       choice is singular use of &ldquo;they,&rdquo;
       &ldquo;them&rdquo; and &ldquo;their.&rdquo; Another choice uses
       the gender-neutral singular pronouns, &ldquo;person,&rdquo;
       &ldquo;per&rdquo; and &ldquo;pers,&rdquo; which are used in
       <a href="/prep/maintain/maintain.html#About-This-Document">
       Information for Maintainers of GNU Software</a>.
       Other gender-neutral pronouns have also been used in English.
    </p>
  </li>
</ol>
</div>

</div><!-- for id="content", starts in the include above -->
<!--#include virtual="/server/footer.html" -->
<div id="footer" role="contentinfo">
<div class="unprintable">

<p>Please send general FSF &amp; GNU inquiries to
<a href="mailto:gnu@gnu.org">&lt;gnu@gnu.org&gt;</a>.
There are also <a href="/contact/">other ways to contact</a>
the FSF.  Broken links and other corrections or suggestions can be sent
to <a href="mailto:webmasters@gnu.org">&lt;webmasters@gnu.org&gt;</a>.</p>

<p><!-- TRANSLATORS: Ignore the original text in this paragraph,
        replace it with the translation of these two:

        We work hard and do our best to provide accurate, good quality
        translations.  However, we are not exempt from imperfection.
        Please send your comments and general suggestions in this regard
        to <a href="mailto:web-translators@gnu.org">
        &lt;web-translators@gnu.org&gt;</a>.</p>

        <p>For information on coordinating and contributing translations of
        our web pages, see <a
        href="/server/standards/README.translations.html">Translations
        README</a>. -->
Please see the <a
href="/server/standards/README.translations.html">Translations
README</a> for information on coordinating and contributing translations
of this article.</p>
</div>

<!-- Regarding copyright, in general, standalone pages (as opposed to
     files generated as part of manuals) on the GNU web server should
     be under CC BY-ND 4.0.  Please do NOT change or remove this
     without talking with the webmasters or licensing team first.
     Please make sure the copyright date is consistent with the
     document.  For web pages, it is ok to list just the latest year the
     document was modified, or published.
     
     If you wish to list earlier years, that is ok too.
     Either "2001, 2002, 2003" or "2001-2003" are ok for specifying
     years, as long as each year in the range is in fact a copyrightable
     year, i.e., a year in which the document was published (including
     being publicly visible on the web or in a revision control system).
     
     There is more detail about copyright years in the GNU Maintainers
     Information document, www.gnu.org/prep/maintain. -->

<p>Copyright &copy; 2018-2021 Free Software Foundation, Inc.</p>


<p>This page is licensed under a <a rel="license"
href="http://creativecommons.org/licenses/by-nd/4.0/">Creative
Commons Attribution-NoDerivatives 4.0 International License</a>.</p>

<!--#include virtual="/server/bottom-notes.html" -->

<p class="unprintable">Updated:
<!-- timestamp start -->
$Date: 2021/09/16 16:30:32 $
<!-- timestamp end -->
</p>
</div>
</div><!-- for class="inner", starts in the banner include -->
</body>
</html>
