<!--#set var="ENGLISH_PAGE" value="/philosophy/network-services-arent-free-or-nonfree.en.html" -->

<!--#include virtual="/server/header.es.html" -->
<!-- Parent-Version: 1.96 -->
<!-- This page is derived from /server/standards/boilerplate.html -->
<!--#set var="TAGS" value="essays cultural ns" -->
<!--#set var="DISABLE_TOP_ADDENDUM" value="yes" -->

<!-- This file is automatically generated by GNUnited Nations! -->
<title>Los servicios en línea no son ni libres ni privativos. Plantean otros
problemas - Proyecto GNU - Free Software Foundation</title>

<!--#include virtual="/philosophy/po/network-services-arent-free-or-nonfree.translist" -->
<!--#include virtual="/server/banner.es.html" -->
<!--#include virtual="/philosophy/ph-breadcrumb.es.html" -->
<!--GNUN: OUT-OF-DATE NOTICE-->
<!--#include virtual="/server/top-addendum.es.html" -->
<div class="article reduced-width">
<h2>Los servicios en línea no son ni libres ni privativos. Plantean otros
problemas</h2>

<address class="byline">por <a href="https://www.stallman.org/">Richard Stallman</a></address>

<div class="introduction">
<p><em>Los programas y los servicios son dos cosas diferentes. Un programa es
un tipo de obra que se puede ejecutar; un servicio es una actividad con la
cual se puede interactuar.</em></p>
</div>

<p>Hacemos una distinción entre programas libres y programas que no son libres
(privativos). Más precisamente, esta distinción se aplica a un programa del
cual se posee una copia: o <a href="/philosophy/free-sw.html">se tienen las
cuatro libertades para esa copia</a> o no se tienen. Si no se tienen, ese
programa está cometiendo una injusticia con usted solo por el hecho de no
ser libre.</p>

<p>Los propietarios del copyright de un programa que no es libre pueden
subsanar esa injusticia de una manera bien simple: publicando el mismo
código fuente bajo una licencia de software libre. Convencerlos de hacerlo
así puede ser difícil, pero es bueno intentarlo.</p>

<p>Una actividad (por ejemplo un servicio) no existe en forma de copias, de
manera que los usuarios no tienen la posibilidad de poseer copias de la
misma, y mucho menos hacer más copias. Si no se dispone de una copia que
modificar, el usuario no puede modificar nada. En consecuencia, las cuatro
libertades que definen el software libre carecen de sentido en el caso de
los servicios. No tiene sentido decir que un servicio es «libre» o que «no
es libre». Esa distinción no es aplicable a los servicios.</p>

<p>Eso no significa que el servicio trate a los usuarios de forma justa. Todo
lo contrario, muchos servicios dañan a los usuarios de diversas maneras, y
los llamamos malservicios, pero no existe una manera sencilla de solucionar
esto, como en el caso de los programas que no son libres (publicarlos como
software libre, de modo que los usuarios puedan ejecutarlos y tener el
control de sus copias y versiones).</p>

<p>Para usar una analogía culinaria, mi manera de cocinar no puede ser una
copia de su manera de cocinar, ni siquiera si yo hubiera aprendido a cocinar
observándole a usted. Yo podría tener y usar una copia de la misma
<em>receta</em> que usted usa para cocinar, porque una receta &mdash;al
igual que un programa&mdash; es una obra y existe en forma de copias, pero
la receta no es lo mismo que su manera de cocinar (y ninguna de las dos
equivale a la comida producida por su acción de cocinar).</p>

<p>Con las tecnologías de hoy día, los servicios a menudo se implementan
ejecutando programas en computadoras, pero esa no es la única manera de
implementarlos (de hecho hay servicios de red que se implementan pidiéndole
a seres humanos que ingresen respuestas a ciertas preguntas). En todo caso,
los usuarios del servicio no visualizan la implementación, de modo que no
tiene efecto directo sobre ellos.</p>

<p>Un servicio de red puede presentar a los usuarios problemas de disyuntiva
entre software libre y software que no es libre con respecto al programa
cliente necesario para utilizar el servicio. Si el servicio requiere
utilizar un programa cliente que no es libre, el uso del servicio requiere
ceder su libertad a ese programa. En muchos servicios web, el software que
no es libre es <a href="/philosophy/javascript-trap.html"> código
JavaScript</a> que se instala silenciosamente en el navegador del
usuario. El programa  <a href="/software/librejs">LibreJS de GNU</a> permite
rechazar fácilmente la ejecución de código JavaScript que no es libre. Pero
el problema del software cliente está lógicamente separado del servicio como
tal.</p>

<p>Hay un caso en el que un servicio es directamente equiparable a un programa:
cuando utilizar el servicio equivale a tener una copia de un hipotético
programa y a ejecutarlo uno mismo. En ese caso lo llamamos <i>servicio
sustitutivo del software</i>, o <abbr title="Service as a Software
Substitute">SaaSS</abbr> (acuñamos este término por ser menos vago y general
que «software como servicio»). Tal servicio es siempre dañino: realiza las
tareas de computación del usuario, mientras son los usuarios quienes deben
tener el control absoluto de esas tareas. Para poder ejercer el control de
sus tareas de computación, el usuario tiene que realizarlas ejecutando su
propia copia de un programa libre.  Utilizar un servidor ajeno para realizar
tareas de computación implica la pérdida de control.</p>

<p>Usar el SaaSS equivale a usar  un programa que no es libre, con funciones de
vigilancia y con puerta trasera universal, de manera que <a
href="/philosophy/who-does-that-server-really-serve.html">debemos rechazarlo
y reemplazarlo por un programa libre</a> que sirva para realizar la misma
tarea.</p>

<p>Sin embargo, las funciones de los principales servicios consisten en
comunicar o publicar información. No tienen nada que ver con ejecutar un
programa uno mismo, de manera que no son un <i>servicio sustitutivo del
software</i>. Tampoco podrían ser reemplazados por una copia suya del
programa. Un programa que se ejecuta en su propia computadora, utilizada
únicamente por usted y aislada de otras, no se comunica con nadie más.</p>

<p>Un servicio que no sea SaaSS puede maltratar a los usuarios haciéndoles algo
injusto en concreto. Por ejemplo, podría hacer mal uso de los datos que
envían los usuarios, o recopilar demasiados datos (vigilancia). También
podría estar diseñado para confundir o engañar a los usuarios (por ejemplo,
mediante «patrones oscuros»). O podría imponer condiciones de uso injustas o
antisociales. El <a
href="https://web.archive.org/web/20090124084811/http://autonomo.us/2008/07/franklin-street-statement/"><cite>Franklin
Street Statement</cite></a> intentó ocuparse de estos asuntos, pero aún no
tenemos completo conocimiento de ellos. Lo que está claro es que los
problemas que presenta un servicio son <em>diferentes</em> a los de un
programa. Por lo tanto, en aras de la claridad, es mejor no aplicar los
términos «libre» y «privativo» a un servicio.</p>

<p>Supongamos que se implemente un servicio utilizando software: El operador
del servidor tiene copias de muchos programas y los ejecuta para implementar
el servicio. Esas copias pueden o no ser software libre. Si el operador las
desarrolló y las utiliza sin distribuir copias, son libres en sentido
trivial ya que cada usuario (sólo hay uno) tiene las cuatro libertades.</p>

<p>Si alguna de las copias no es libre, eso generalmente no afecta directamente
a los usuarios del servicio pues ellos no están ejecutando esos programas,
es el operador del servicio quien lo hace. Hay una situación particular en
la que estos programas pueden afectar indirectamente a los usuarios del
servicio: si el servicio conserva información privada, los usuarios podrían
manifestar preocupación ante la posibilidad de que los programas
propietarios del servidor tengan puertas traseras que permitan a terceros
acceder a sus datos. De hecho, los programas propietarios del servidor
requieren que los usuarios confíen en los desarrolladores de los programas
como así también en el operador del servicio. Las implicaciones de esto en
la práctica dependen de cada caso, por ejemplo el tipo de tareas que
efectúan los programas que no son libres.</p>

<p>Sin embargo, la parte que <em>ciertamente</em> se ve perjudicada por los
programas privativos que implementan el servicio es precisamente el operador
del servidor. No lo condenamos por estar a merced del software que no es
libre, y ciertamente no lo boicoteamos por eso. Más bien, nos preocupa su
libertad, al igual que la de cualquier otro usuario de software que no es
libre. Siempre que tenemos ocasión, tratamos de explicarle que está
limitando su libertad, con la esperanza de que se vuelque hacia el software
libre.</p>

<p>Por otro lado, si el operador del servicio ejecuta GNU/Linux o cualquier
software libre, más que una virtud que le afecta a usted, es un beneficio
para él o para ella. Nosotros no lo alabaremos ni le daremos las gracias por
eso, más bien lo felicitaremos por su sabia elección. </p>

<p>Si el operador ha desarrollado algún programa para el servicio y lo ha
publicado como software libre, allí sí tendremos una razón para
agradecerle. Sugerimos distribuir esos programas bajo la licencia <a
href="/licenses/license-recommendations.html">GPL Affero de GNU </a>, ya que
evidentemente son útiles en los servidores.</p>

<p><a href="/licenses/why-affero-gpl.html">¿Por qué la GPL Affero?</a></p>

<p>Así, no tenemos ninguna regla que indique que los sistemas libres no deben
usar (o no deben depender de) los servicios (o sitios) implementados con
software que no es libre. Sin embargo, no tienen que depender de, sugerir ni
alentar el uso de servicios tales como el SaaSS. Es necesario utilizar
software libre en lugar de SaaSS. Siendo los demás factores iguales, es
bueno favorecer a aquellos proveedores de servicios que contribuyen a la
comunidad publicando software libre útil y, para aquellas actividades que no
requieren necesariamente un nodo centralizado, es también bueno favorecer
las comunicaciones entre pares (<abbr title="peer-to-peer">P2P</abbr>) en
lugar de la comunicación centralizada basada en servidores.</p>
</div>

<div class="translators-notes">

<!--TRANSLATORS: Use space (SPC) as msgstr if you don't have notes.-->
 </div>
</div>

<!-- for id="content", starts in the include above -->
<!--#include virtual="/server/footer.es.html" -->
<div id="footer" role="contentinfo">
<div class="unprintable">

<p>Envíe sus consultas acerca de la FSF y GNU a <a
href="mailto:gnu@gnu.org">&lt;gnu@gnu.org&gt;</a>. Existen también <a
href="/contact/">otros medios para contactar</a> con la FSF. <br /> Para
avisar de enlaces rotos y proponer otras correcciones o sugerencias,
diríjase a <a
href="mailto:webmasters@gnu.org">&lt;webmasters@gnu.org&gt;</a>.</p>

<p>
<!-- TRANSLATORS: Ignore the original text in this paragraph,
        replace it with the translation of these two:

        We work hard and do our best to provide accurate, good quality
        translations.  However, we are not exempt from imperfection.
        Please send your comments and general suggestions in this regard
        to <a href="mailto:web-translators@gnu.org">

        &lt;web-translators@gnu.org&gt;</a>.</p>

        <p>For information on coordinating and contributing translations of
        our web pages, see <a
        href="/server/standards/README.translations.html">Translations
        README</a>. -->
El equipo de traductores al español se esfuerza por ofrecer traducciones
fieles al original y de buena calidad, pero no estamos libres de cometer
errores.<br /> Envíe sus comentarios y sugerencias sobre las traducciones a
<a
href="mailto:web-translators@gnu.org">&lt;web-translators@gnu.org&gt;</a>.
</p><p>Consulte la <a href="/server/standards/README.translations.html">Guía
para las traducciones</a> para obtener información sobre la coordinación y
el envío de traducciones de las páginas de este sitio web.</p>
</div>

<!-- Regarding copyright, in general, standalone pages (as opposed to
     files generated as part of manuals) on the GNU web server should
     be under CC BY-ND 4.0.  Please do NOT change or remove this
     without talking with the webmasters or licensing team first.
     Please make sure the copyright date is consistent with the
     document.  For web pages, it is ok to list just the latest year the
     document was modified, or published.
     
     If you wish to list earlier years, that is ok too.
     Either "2001, 2002, 2003" or "2001-2003" are ok for specifying
     years, as long as each year in the range is in fact a copyrightable
     year, i.e., a year in which the document was published (including
     being publicly visible on the web or in a revision control system).
     
     There is more detail about copyright years in the GNU Maintainers
     Information document, www.gnu.org/prep/maintain. -->
<p>Copyright &copy; 2012-2014, 2020, 2021 Free Software Foundation, Inc.</p>

<p>Esta página está bajo licencia <a rel="license"
href="http://creativecommons.org/licenses/by-nd/4.0/deed.es_ES">Creative
Commons Reconocimiento-SinObraDerivada 4.0 Internacional</a>.</p>

<!--#include virtual="/server/bottom-notes.es.html" -->
<div class="translators-credits">

<!--TRANSLATORS: Use space (SPC) as msgstr if you don't want credits.-->
<strong>Traducción: SirGuide, 2012.</strong> Revisión: Equipo de traductores
al español de GNU.</div>

<p class="unprintable"><!-- timestamp start -->
Última actualización:

$Date: 2021/09/24 20:32:21 $

<!-- timestamp end -->
</p>
</div>
</div>
<!-- for class="inner", starts in the banner include -->
</body>
</html>
