This implements the 'shop.demo.taler.net' site.

ISSUES:
=======

To add another language, you currently MUST edit the list of languages
at the end of content.py as well as the templates/base.html.j2
