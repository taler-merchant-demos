##
# This file is part of GNU TALER.
# Copyright (C) 2024 Taler Systems SA
#
# TALER is free software; you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free Software
# Foundation; either version 2.1, or (at your option) any later version.
#
# TALER is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License along with
# GNU TALER; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
#
# @author Florian Dold

import os
from .util.talerconfig import TalerConfig

def load_taler_config():
    # The (WSGI) host passes the configuration endpoint via
    # an environment variable.
    config_filename = os.environ["TALER_CONFIG_FILENAME"]
    if config_filename == "":
        config_filename = None
    return TalerConfig.from_file(config_filename)
