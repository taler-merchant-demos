install_global=false
-include .config.mk

version := $(shell poetry version | awk '{ print $$2 }')

.PHONY: all
all:
	@echo "This is a python project, no compilation required"
	@echo "Installation: make install"

setup-arch:
# TODO: We should probabally add some form of "ignore me if already completed"
	set -e
	echo "WARN: This script has not been tested! Use with caution."
	echo "Upgrading System & Installing Python..."
	sudo pacman -Syu --noconfirm python python-pip python-flask-babel python-poetry python-babel
	echo "Configuring..."
	./configure --prefix=local
	echo "Installing Pip Dependencies..."
	echo "-> Installing wheel..."
	pip install wheel
	echo "-> Installing lxml..."
	pip install lxml
	echo "-> Installing poetry..."
	pip install poetry
	echo "Done!"

setup-deb:
# TODO: We should probabally add some form of "ignore me if already completed"
	set -e
	echo "WARN: This script has not been tested! Use with caution."
	echo "Updating Package Repositories..."
	sudo apt update -y
	echo "Installing python, python-flask-babel, python-pip, python-is-python3..."
	sudo apt install -y python3 python3-flask-babel python3-pip python-is-python3
	echo "Configuring..."
	./configure --prefix=local
	echo "Installing Pip Dependencies..."
	echo "-> Installing wheel..."
	pip install wheel
	echo "-> Installing lxml..."
	pip install lxml
	echo "-> Installing poetry..."
	pip install poetry
	echo "Done!"

my_venv = $(DESTDIR)$(prefix)/lib/taler-merchant-demos/venv
bindir = $(DESTDIR)$(prefix)/bin

.PHONY: install
install: compile
	mkdir -p $(bindir)
	poetry build -f wheel
	python3 -m venv $(my_venv)
	$(my_venv)/bin/pip3 install --upgrade --ignore-installed "dist/talermerchantdemos-$(version)-py3-none-any.whl"
	ln -sf ../lib/taler-merchant-demos/venv/bin/taler-merchant-demos $(bindir)/taler-merchant-demos

# run testcases
.PHONY: check
check:
	@export TALER_CONFIG_FILE=talerblog/tests.conf; \
        python3 setup.py test

.PHONY: clean
clean:
	@echo nothing to do

.PHONY: dist
dist:
	git archive --format=tar.gz HEAD -o taler-merchant-blog.tar.gz

.PHONY: pretty
pretty:
	black talermerchantdemos

# i18n
extract:
#	Note: Flask-BabelEx expects 'translations/' for the dirname,
#       even though GNU gettext typically uses 'locale/'
	mkdir -p talermerchantdemos/translations
	pybabel extract -F babel.cfg -o talermerchantdemos/translations/messages.pot .
#	Add new language as follows:
#	pybabel init -i locale/messages.pot -d locale/ -l de

.PHONY: compile
compile: update
	pybabel compile -d talermerchantdemos/translations

.PHONY: update
update: extract
	pybabel update -i talermerchantdemos/translations/messages.pot -d talermerchantdemos/translations

